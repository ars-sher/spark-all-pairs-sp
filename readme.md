Run examples:

* cd to app root folder
* mvn package
* ~/spark-1.2.0/bin/spark-submit --class com.allpairs.Main --master spark://Arsmint:7077 target/spark-all-pairs-sp-0.1.jar /home/ars/data/test 3

local run:

* mvn package
* mvn exec:java -Dexec.mainClass=com.allpairs.Main -Dexec.args="/home/ars/data/test 3"