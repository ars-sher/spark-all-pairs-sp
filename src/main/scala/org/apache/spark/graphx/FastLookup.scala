package org.apache.spark.graphx

import scala.reflect.ClassTag

object FastLookup {
  // fast unsupported way to do it
  // see http://apache-spark-user-list.1001560.n3.nabble.com/GraphX-How-to-access-a-vertex-via-vertexId-td10852.html
  def getVertexAttrByID[VD: ClassTag, ED: ClassTag](graph: Graph[VD, ED], vid: VertexId): VD = {
    val vertexRDD = graph.vertices
    val result = vertexRDD.partitionsRDD.flatMap { part =>
      if (part.isDefined(vid)) Some(part(vid))
      else None
    }.collect()
    if (result.isEmpty)
      throw new Exception("attribute not found, wrong vertex id")
    else
      result.head
  }
}
