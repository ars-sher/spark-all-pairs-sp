package com.allpairs

import org.apache.spark

trait Logging extends spark.Logging{
  override protected def logWarning(msg: => String) {
    super.logWarning(msg)
  }

  override protected def logInfo(msg: => String) {
    super.logInfo(msg)
  }
}
