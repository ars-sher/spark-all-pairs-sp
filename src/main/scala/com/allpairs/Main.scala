package com.allpairs

import com.allpairs.Draw.DrawGraph
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD
import org.apache.spark.graphx._
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.graphx.FastLookup

import Math._

import scala.reflect.ClassTag
import scala.collection.mutable

object Main extends Logging {
  def main(args: Array[String]) {
    if (args.length < 2) {
      val className = this.getClass.getName.stripSuffix("$")
      println(
        s"""
          |usage (local run): mvn exec:java -Dexec.mainClass=$className -Dexec.args="<path/to/graph> <k>"
          |input file must be in adjacency list format
          |seed sets will be sampled k times
        """.stripMargin)
      System.exit(1)
    }
    val graphPath = args(0)
    val k = args(1).toInt
    val appName = "all-pairs sp"

    val localMaster = "local[1]"
    val conf = new SparkConf().setAppName(appName)
    // check whether local run or not
    // spark-submit will automatically set spark.master in cluster run
    val master = conf.get("spark.master", localMaster)
    val localRun = master.startsWith("local")
    if (localRun)
      conf.setMaster(master)

    val sc = new SparkContext(conf)
    // suppress logging
    Logger.getLogger("org").setLevel(Level.WARN)
    Logger.getLogger("akka").setLevel(Level.WARN)

    val graph = GraphLoader.edgeListFile(sc, graphPath).cache()
    // uncomment it to see initial graph
//    DrawGraph.drawGraph(graph.triplets.collect(), "initial")

    val precomputedGraph = precompute(sc, graph, k).cache()

    getShortestPath(precomputedGraph, 0, 5)
    getShortestPath(precomputedGraph, 5, 0)
    getShortestPath(precomputedGraph, 8, 10)

    if (localRun)
      System.exit(0)
  }

  // calculate and print seed sets
  def getSeedSets[VD, ED](graph: Graph[VD, ED]): Array[Set[VertexId]] = {
    val n = graph.vertices.count()
    val r = floor(Math.log(n)).toInt
    val seedSetSizes = Array.fill(r + 1)(0)
    for(i <- 0 until seedSetSizes.length) {
      seedSetSizes(i) = 1 << i
    }
    val seedSets = seedSetSizes.map(seedSize => graph.vertices.takeSample(withReplacement = false, num = seedSize).map(_._1).toSet)
    println("______________________________________")
    seedSets.foreach(seedSet => {
      println("Next seed set:")
      seedSet.foreach(println(_))
    })
    seedSets
  }

  // precompute sketches for each vertex
  def precompute[VD: ClassTag, ED: ClassTag](sc:SparkContext, graph: Graph[VD, ED], k: Int): Graph[Sketch, ED] = {
    // proof-of-concept
    // val setExample = Set[Long](10, 11)
    // val v: VertexId = 2
    // val (splength, sppath, lastVertex) = SetSP.run(graph, v, setExample, reverseDirection = true)
    // println(s"shortest path from $setExample to $v is " + SetSP.path_to_nice_string(sppath) + s", having length $splength, last vertex is $lastVertex")

    val sketchGraphEmpty = graph.mapVertices((id, attr) => new Sketch())
    val localVerticesEmptySketch = sketchGraphEmpty.vertices.collect()
    val edgeRDD = sketchGraphEmpty.edges

    var localVerticesSketch = localVerticesEmptySketch
    for (i <- 1 to k) {
      logInfo(s"Calculating sketches, $i round...")
      val seedSets = getSeedSets(graph)
      localVerticesSketch =
        localVerticesSketch.map { case (vid: VertexId, sketch: Sketch) => (vid, getSketch(graph, vid, seedSets, sketch))}
    }

    val vertexRDD = VertexRDD(sc.parallelize(localVerticesSketch))
    val sketchGraph = Graph(vertexRDD, edgeRDD)
    // uncomment it to see sketches
//    DrawGraph.drawGraph(sketchGraph.triplets.collect(), "sketch")
    logInfo("Precomputing finished")
    sketchGraph
  }

  // update sketch oldSketch of vertex vid with seedSets
  def getSketch[VD: ClassTag, ED: ClassTag](graph: Graph[VD, ED],
                                            vid: VertexId,
                                            seedSets: Array[Set[VertexId]],
                                            oldSketch: Sketch): Sketch = {
    val sketch = new Sketch(oldSketch.pathsTo, oldSketch.pathsFrom) // old values

    for (seedSet <- seedSets) {
      val (splengthTo, sppathTo, landmarkTo) = SetSP.run(graph, vid, seedSet)
      if (splengthTo != -1) {
        updatePaths(landmarkTo, splengthTo, sppathTo, sketch.pathsTo)
      }
      val (splengthFrom, sppathFrom, landmarkFrom) = SetSP.run(graph, vid, seedSet, reverseDirection = true)
      if (splengthFrom != -1) {
        updatePaths(landmarkFrom, splengthFrom, sppathFrom, sketch.pathsFrom)
      }
    }
    sketch
  }

  // if no such path in map, put it there; if path is already exists, put minimum
  def updatePaths(vidToPut: VertexId,
                   splength: Long, sppath: List[VertexId],
                   paths: mutable.Map[VertexId, (Long, List[VertexId])]): Unit = {
    if (paths.contains(vidToPut)) {
      val (minLenght, minPath) = if (splength < paths(vidToPut)._1) {
        (splength, sppath)
      } else (paths(vidToPut)._1, paths(vidToPut)._2)
      paths(vidToPut) = (minLenght, minPath)
    } else {
      paths(vidToPut) = (splength, sppath)
    }
  }

  // calculate shortest path using graph with ready sketches
  def getShortestPath[ED: ClassTag](graph: Graph[Sketch, ED],
                                    from: VertexId, where: VertexId): (Long, List[VertexId]) = {
    val firstHalfCandidates = FastLookup.getVertexAttrByID(graph, from).pathsFrom
    val lastHalfCandidates = FastLookup.getVertexAttrByID(graph, where).pathsTo
    val commonLandmarks = firstHalfCandidates.keySet intersect lastHalfCandidates.keySet
    val res =
    if (commonLandmarks.isEmpty) {
      logInfo(s"Shortest path from $from to $where not found, they have no common landmarks in this direction")
      (-1.toLong, List[VertexId]())
    }
    else {
      var minPathLength = Long.MaxValue
      var minPath = List[VertexId]()
      for (landmark <- commonLandmarks) {
        val pathLength = firstHalfCandidates(landmark)._1 + lastHalfCandidates(landmark)._1
        if (minPathLength > pathLength) {
          minPathLength = pathLength
          minPath = firstHalfCandidates(landmark)._2 ::: lastHalfCandidates(landmark)._2.tail
        }
      }
      val minPathFormatted = SetSP.path_to_nice_string(minPath)
      logInfo(s"Found shortest path from $from to $where with length $minPathLength: $minPathFormatted")
      (minPathLength, minPath)
    }
    res
  }
}

// maps have structure (path_where -> (path_length, path))
class Sketch(val pathsTo: mutable.Map[VertexId, (Long, List[VertexId])],
             val pathsFrom: mutable.Map[VertexId, (Long, List[VertexId])]) extends Serializable {
  def this() = this(mutable.Map[VertexId, (Long, List[VertexId])](), mutable.Map[VertexId, (Long, List[VertexId])]())

  override def toString = s"to: $pathsTo, from: $pathsFrom"
}
