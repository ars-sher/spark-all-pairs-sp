package com.allpairs

import com.allpairs.Draw.DrawGraph
import org.apache.spark.graphx
import org.apache.spark.graphx._
import org.apache.spark.graphx.FastLookup
import scala.reflect.ClassTag
import scala.collection.mutable.ListBuffer

object SetSP extends Logging{
  // Accepts graph, a set of vertex ids and id of vertex v. The function tries to find a shortest path from
  // any of the given set's vertices to v in natural or reversed direction and returns path's length with itself + last vertexId
  // returns (-1, List.empty()) if no path exists
  def run[VD: ClassTag, ED: ClassTag](graph: Graph[VD, ED],
                                      vid: VertexId,
                                      seedSet: Set[VertexId],
                                      reverseDirection: Boolean = false): (Long, List[VertexId], VertexId) = {
    if (!reverseDirection) {
      val (sp_length, sp, lastVertex) = run_direct(graph, vid, seedSet)
      (sp_length, sp.reverse, lastVertex)
    } else {
      run_direct(graph.reverse, vid, seedSet)
    }
  }

  def run_direct[VD: ClassTag, ED: ClassTag](graph: Graph[VD, ED],
                                             vid: VertexId,
                                             seedSet: Set[VertexId]): (Long, List[VertexId], VertexId) = {
    val spGraph = graph.mapVertices((id, _) => SPVertex.newVertex(id, seedSet)) // initialize graph

    // Define the three functions needed to implement SP in the GraphX's Pregel
    def vertexProgram(id: VertexId, attr: SPVertex, msgSum: SPCombiner): SPVertex = {
      val newAttr = if (msgSum._1 < attr.path_length) {
        if (id == vid) { // finally have found source vertex, halting execution
          new SPVertex(msgSum._2, msgSum._1 + 1, true)
        }
        else new SPVertex(msgSum._2, msgSum._1 + 1)
      }
      else attr
      newAttr
    }

    // we message to dst vertex if our path + 1 is less then dst's
    def sendMessage(edge: EdgeTriplet[SPVertex, ED]): Iterator[(VertexId, SPCombiner)] = {
      if (edge.srcAttr.path_length < edge.dstAttr.path_length - 1)
        Iterator((edge.dstId, (edge.srcAttr.path_length, edge.srcId)))
      else
        Iterator.empty
    }

    // choose min length path
    def messageCombiner(a: SPCombiner, b: SPCombiner): SPCombiner = if (a._1 < b._1) a else b

    def haltExecution(g: Graph[SPVertex, ED]): Boolean = {
      val halted = g.vertices.filter{ case (id, attr) => attr.haltExecution}.count()
      halted == 1
    }

    // The initial message received by all vertices
    val initialMessage = (Long.MaxValue, -1.toLong)

    // Execute shortest path
    val calculatedSPGraph = com.allpairs.Pregel(spGraph, initialMessage, activeDirection = EdgeDirection.Out)(
                                             vertexProgram, sendMessage, messageCombiner, haltExecution)
    // uncomment it to see set-shortest-path graph
    // DrawGraph.drawGraph(calculatedSPGraph.triplets.collect(), "sp")

    val v_attr = FastLookup.getVertexAttrByID(calculatedSPGraph, vid)
    if (v_attr.predecessor == -1.toLong) {
      (-1, List[VertexId](), -1)
    } else {
      val sp_length = v_attr.path_length
      val (sp, lastVertex) = collectSP(calculatedSPGraph, vid, seedSet)
      (sp_length, sp, lastVertex)
    }
  }

  // current shortest path and predecessor's id
  type SPCombiner = (Long, VertexId)

  // this is slow, officially supported way to get attribute by id.
  // see http://apache-spark-user-list.1001560.n3.nabble.com/GraphX-How-to-access-a-vertex-via-vertexId-td10852.html
  def getVertexAttrByIDSafe[VD, ED](graph: Graph[VD, ED], vid: VertexId): VD = {
    val arrWithAttr = graph.vertices.filter{case(id, _) => id==vid}.collect()
    if (arrWithAttr.isEmpty)
      throw new Exception("attribute not found, wrong vertex id")
    else
      arrWithAttr(0)._2
  }

  // traverse graph from source vertex and collect shortest path, returning (shortest_path, last_vertex)
  def collectSP[ED: ClassTag](graph: Graph[SPVertex, ED], vid: VertexId, seedSet: Set[VertexId]): (List[VertexId], VertexId) = {
    val buf = new ListBuffer[VertexId]
    var nextVertexId = vid
    while (! seedSet.contains(nextVertexId)) {
      buf += nextVertexId
      nextVertexId = FastLookup.getVertexAttrByID(graph, nextVertexId).predecessor
    }
    buf += nextVertexId
    (buf.toList, nextVertexId)
  }

  def path_to_nice_string(path: List[VertexId]): String = path mkString "->"
}

class SPVertex(val predecessor: VertexId,
               val path_length: Long,
               val haltExecution: Boolean = false) extends Serializable {

  private val lenght_to_show = if (path_length == Long.MaxValue) -2 else path_length // just show -2 instead of infinity
  override def toString: String = s"p: $predecessor, pl: $lenght_to_show, he: $haltExecution"
}

object SPVertex {
  def newVertex(thisVId: VertexId, seedSet: Set[VertexId]): SPVertex = {
    if (seedSet contains thisVId) {
      new SPVertex(-1, 0)
    }
    else {
      // strictly speaking, there should be something like Double.PositiveInfinity, but I don't think we will encounter any problems
      new SPVertex(-1, Long.MaxValue)
    }
  }
}