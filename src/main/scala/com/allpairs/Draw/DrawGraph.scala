package com.allpairs.Draw

import java.io.{File, PrintWriter}

import org.apache.spark.graphx.EdgeTriplet
import org.apache.spark.rdd.RDD

import scala.sys.process._

// simple object for drawing graphs
// graphviz must be installed

object DrawGraph {
  def makeDotFileFromTriplets[VD, ED](triplets: Array[EdgeTriplet[VD, ED]], filename: String): Unit = {
    val writer = new PrintWriter(new File(filename))
    writer.write("digraph SimpleGraph {\n    rankdir=LR;\n    node [shape=circle];\n    ranksep=0.5;\n")
    writer.write("    node [style=solid];\n")
    val tripletsSortedBySrcVID = triplets.sortWith((a, b) => a.srcId < b.srcId)
    tripletsSortedBySrcVID.foreach(t => {
      writer.write("    \"" + t.srcId + " ; " + t.srcAttr + "\"" + " -> " +
        "\"" + t.dstId + " ; " + t.dstAttr + "\"" + " [label=\"" + t.attr + "\"];\n")
    })

    writer.write("}\n")
    writer.close()
  }

  def drawGraphByDotFile(gvFileName: String, pngFileName: String): Int = {
    val cmd = "dot -T png " + gvFileName
    val resCode = (cmd #> new java.io.File(pngFileName)).!
    resCode
  }

  // draw graphx graph
  // doesn't draw dangling vertices!
  // accepts graph.triplets.collect as an input graph
  def drawGraph[VD, ED](triplets: Array[EdgeTriplet[VD, ED]], name: String, removeDotFile: Boolean = true): Int = {
    val gvname = name + ".gv"
    val pngname = name + ".png"
    makeDotFileFromTriplets(triplets, gvname)
    val res = drawGraphByDotFile(gvname, pngname)
    if (removeDotFile) {
      val deleteRes = ("rm " + gvname).!
    }
    return res
  }
}
